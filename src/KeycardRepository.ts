import { Keycard } from "./entity/Keycard";
import { EntityRepository, Repository, In } from "typeorm";
import { DatabaseError } from "./DatabaseError";
import { SystemError } from "./SystemError";

@EntityRepository(Keycard)
export class KeycardRepository extends Repository<Keycard> {
  public async createKeycards(totalNumberOfRooms: number, hotelId: number): Promise<Keycard[]> {
    const keycardList = range(1, totalNumberOfRooms)
      .map((keycardNumberIndex) => {
        const keycardNumber = keycardNumberIndex;

        return this.create({
          hotelId: hotelId,
          number: keycardNumber.toString(),
          roomNumber: undefined
        })
      });

    try {
      return await this.save(keycardList);
    } catch (error) {
      throw new DatabaseError("Error in function createKeycards, error message: " + error.message);
    }
  }

  public async availableKeycard(hotelId: number): Promise<Keycard> {
    try {
      return await this.findOneOrFail({
        where: {
          roomNumber: null,
          hotelId: hotelId
        },
        order: {
          number: 'ASC'
        }
      })
    } catch (error) {
      throw new DatabaseError("Error in function availableKeycard, error message: " + error.message);
    }
  }

  public async getAvailableKeycardList(hotelId: number, numberOfKeycards: number): Promise<Keycard[]> {
    try {
      return await this.find({
        where: {
          roomNumber: null,
          hotelId: hotelId
        },
        order: {
          number: 'ASC'
        },
        take: numberOfKeycards
      })
    } catch (error) {
      throw new DatabaseError("Error in function getAvailableKeycardList, error message: " + error.message);
    }
  }

  public async findKeycardByCondition(condition: string, number: string, hotelId: number): Promise<Keycard> {
    if (condition === "keycardNumber") {
      return await this.findOneOrFail({
        number: number,
        hotelId: hotelId
      })
    }
    else if (condition === "roomNumber") {
      return await this.findOneOrFail({
        roomNumber: number,
        hotelId: hotelId
      })
    }
    else {
      throw new SystemError(`Couldn't find keycard ${number} by ${condition}`);
    }
  }

  public async findKeycardListByRoomNumberList(roomNumberList: string[], hotelId: number): Promise<Keycard[]> {
    try {
      return await this.find({
        hotelId: hotelId,
        roomNumber: In(roomNumberList)
      })
    } catch (error) {
      throw new DatabaseError("Error in function findKeycardListByRoomNumberList, error message: " + error.message);
    }
  }

  public async saveOne(keycard: Keycard): Promise<void> {
    try {
      this.save(keycard);
    } catch (error) {
      throw new DatabaseError("Error in function saveOne, error message: " + error.message);
    }
  }

  public async saveMany(keycardList: Keycard[]): Promise<void> {
    try {
      this.save(keycardList);
    } catch (error) {
      throw new DatabaseError("Error in function saveMany, error message: " + error.message);
    }
  }

  public async clearOne(keycard: Keycard): Promise<void> {
    try {
      await this.update({
        hotelId: keycard.hotelId,
        number: keycard.number
      }, {
          roomNumber: undefined
        });
    } catch (error) {
      throw new DatabaseError("Error in function clearOne, error message: " + error.message);
    }
  }

  public async clearMany(keycardList: Keycard[]): Promise<void> {
    try {
      const hotelId = keycardList[0].hotelId;
      const keycardNumberList = keycardList.map(keycard => keycard.number);

      await this.update({
        hotelId: hotelId,
        number: In(keycardNumberList)
      }, {
          roomNumber: undefined
        });
    } catch (error) {
      throw new DatabaseError("Error in function clearMany, error message: " + error.message);
    }
  }
}

function range(start: number, end: number) {
  return [...Array(end - start + 1).keys()]
    .map(number => number + start);
}
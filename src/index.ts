import express, { Request, Response, Router, response } from "express"
import bodyParser = require("body-parser");
import { hotelService } from "./HotelService";
import { GuestInfo } from "./entity/GuestInfo";
import { SystemError } from "./SystemError";
import { NextFunction } from "connect";
import { migrateDatabase } from "./migrateDatabase";
import { createConnection } from "typeorm"
import jwt from 'jsonwebtoken'

const envResult = require('dotenv').config()

if (envResult.error) {
  throw new Error(envResult.error);
}

const port = process.env.PORT || 5000

const secretKey = process.env.APP_KEY;

function needAuthorization(
  request: Request,
  response: Response,
  next: NextFunction
) {
  try {
    const [_, token] = request.headers.authorization!.split(' ') as [any, string];

    if (!token) {
      throw new SystemError("No authorization");
    }

    const payload = jwt.verify(token, secretKey!);

    ;(request as any).$payload = payload;
    
  } catch (error) {
    response.status(400).json({ "Authorization error": error.message });
  }

  next();
}

interface HotelIdRequest extends Request {
  $hotelId: string;
}

function withHotelId(
  request: Request,
  response: Response,
  next: NextFunction
) {
  try {
    const [_, hotelId] = request.headers.authorization!.split(' ') as [any, string];
    ; (request as HotelIdRequest).$hotelId = ((request as any).$payload).hotelId;

    next();
  }
  catch(error) {
    response.status(400).json("Probably problem with token, error: " + error.message);
  }
}

const hotelRouter = Router();
hotelRouter.use(bodyParser.json());
hotelRouter.use(needAuthorization, withHotelId);

createConnection().then(async () => {
  console.log("DATABASE: CONNECTED SUCCESSFULLY");

  try {
    migrateDatabase();
  } catch (error) {
    response.json(400).json({ "migrate Database Error": error.message} );
  }

  const app = express();

  app.use(bodyParser.json());

  app.post("/hotels", async (request: Request, response: Response) => {
    const { numberOfFloor, roomsPerFloor } = request.body;

    try {
      const hotel = await hotelService.createHotel(numberOfFloor, roomsPerFloor);

      const token = jwt.sign(
        {
          hotelId: hotel.id.toString()
        },
        secretKey!,
        {
          algorithm: 'HS256', expiresIn: '1h', issuer: 'Hotel'
        }
      )

      return response.json({
        "information": `Hotel created with ${numberOfFloor} floor(s), ${roomsPerFloor} room(s) per floor.`,
        "result": token,
      });
    } catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  hotelRouter.post("/rooms/book", async (request: Request, response: Response) => {
    const { roomNumber, guestName, guestAge } = request.body;

    const hotelRequest = request as HotelIdRequest;
    const hotelId = hotelRequest.$hotelId;

    try {
      const guestInfo = new GuestInfo();
      guestInfo.name = guestName;
      guestInfo.age = guestAge;

      const { bookingDetail, registeredKeycard } = await hotelService.book(roomNumber, guestInfo, parseInt(hotelId));

      return response.json({ "result": { bookingDetail, registeredKeycard } });
    }
    catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  hotelRouter.post("/rooms/checkout", async (request: Request, response: Response) => {
    const { keycardNumber, guestName } = request.body;
    const hotelRequest = request as HotelIdRequest;
    const hotelId = hotelRequest.$hotelId;

    try {
      const checkedOutRoom = await hotelService.checkout(keycardNumber, guestName, parseInt(hotelId));

      return response.json({ "result": checkedOutRoom });
    }
    catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  hotelRouter.post("/rooms/checkout_guest_by_floor", async (request: Request, response: Response) => {
    const { floorNumber } = request.body;
    const hotelRequest = request as HotelIdRequest;
    const hotelId = hotelRequest.$hotelId;

    try {
      const roomList = await hotelService.checkoutOnFloor(floorNumber, parseInt(hotelId));

      return response.json({ "result": roomList });
    }
    catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  hotelRouter.post("/rooms/book_by_floor", async (request: Request, response: Response) => {
    const { floorNumber, guestName, guestAge } = request.body;
    const hotelRequest = request as HotelIdRequest;
    const hotelId = hotelRequest.$hotelId;

    try {
      const guestInfo = new GuestInfo();
      guestInfo.name = guestName;
      guestInfo.age = guestAge;

      const bookingDetailAndKeycardList = await hotelService.bookOnFloor(floorNumber, guestInfo, parseInt(hotelId));

      return response.json({ "result": bookingDetailAndKeycardList });
    }
    catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  hotelRouter.get("/rooms/list_available", async (request: Request, response: Response) => {
    const hotelRequest = request as HotelIdRequest;
    const hotelId = hotelRequest.$hotelId;

    try {
      const roomList = await hotelService.listAvailableRoom(parseInt(hotelId));

      response.json({ "result": roomList });
    } catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  hotelRouter.get("/guests/list", async (request: Request, response: Response) => {
    const hotelRequest = request as HotelIdRequest;
    const hotelId = hotelRequest.$hotelId;

    try {
      const guestList = await hotelService.listGuest(parseInt(hotelId));

      response.json({ "result": guestList });
    } catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  hotelRouter.get("/rooms/get_guest_in_room/:roomNumber", async (request: Request, response: Response) => {
    const hotelRequest = request as HotelIdRequest;
    const hotelId = hotelRequest.$hotelId;
    const { roomNumber } = request.params;

    try {
      const guest = await hotelService.getGuestInRoom(roomNumber, parseInt(hotelId));

      response.json({ "result": guest });
    } catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  hotelRouter.get("/guests/list_guest_by_age/:condition/:targetAge", async (request: Request, response: Response) => {
    const hotelRequest = request as HotelIdRequest;
    const hotelId = hotelRequest.$hotelId;
    const { condition, targetAge } = request.params;

    try {
      const guestList = await hotelService.listGuestByAge(condition, targetAge, parseInt(hotelId));

      response.json({ "result": guestList });
    } catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  hotelRouter.get("/guests/list_guest_by_floor/:floorNumber", async (request: Request, response: Response) => {
    const hotelRequest = request as HotelIdRequest;
    const hotelId = hotelRequest.$hotelId;
    const { floorNumber } = request.params;

    try {
      const guestList = await hotelService.listGuestOnFloor(floorNumber, parseInt(hotelId));

      response.json({ "result": guestList });
    } catch (error) {
      if (error instanceof SystemError) response.status(400).json({ message: error.message });
      else response.status(500).json({ message: error.message });
    }
  })

  app.use(hotelRouter);

  app.listen(port, () => console.log(`App listen on port ${port}.`));
})
import { BookingDetail } from "./entity/BookingDetail"
import { GuestInfo } from "./entity/GuestInfo";
import { Sign } from "./Type";
import { EntityRepository, Repository } from "typeorm";
import * as R from "ramda";
import { DatabaseError } from "./DatabaseError";

@EntityRepository(BookingDetail)
export class BookingDetailRepository extends Repository<BookingDetail> {
  public async findBookingDetailByRoomNumber(roomNumber: string, hotelId: number): Promise<BookingDetail> {
    try {
      return await this.findOneOrFail({
        hotelId: hotelId,
        roomNumber: roomNumber
      })
    } catch (error) {
      throw new DatabaseError("Error in function findBookingDetailByRoomNumber, error message: " + error.message);
    }
  }

  public async addOne(bookingDetail: BookingDetail): Promise<void> {
    try {
      await this.save(bookingDetail);
    } catch (error) {
      throw new DatabaseError("Error in function addOne, error message: " + error.message);
    }
  }

  public async addMany(bookingDetails: BookingDetail[]): Promise<void> {
    try {
      await this.save(bookingDetails);
    } catch (error) {
      throw new DatabaseError("Error in function addMany, error message: " + error.message);
    }
  }

  public async deleteOne(bookingDetail: BookingDetail): Promise<void> {
    try {
      await this.remove(bookingDetail);
    } catch (error) {
      throw new DatabaseError("Error in function deleteOne, error message: " + error.message);
    }
  }

  public async deleteMany(bookingDetails: BookingDetail[]): Promise<void> {
    try {
      await this.remove(bookingDetails);
    } catch (error) {
      throw new DatabaseError("Error in function deleteMany, error message: " + error.message);
    }
  }

  public async getGuestInRoom(roomNumber: string, hotelId: number): Promise<GuestInfo> {
    try {
      const bookingDetail = await this.findOneOrFail({
        roomNumber: roomNumber,
        hotelId: hotelId
      });

      return bookingDetail.guestInfo;
    } catch (error) {
      throw new DatabaseError("Error in function getGuestInRoom, error message: " + error.message);
    }
  }

  public async getBookingDetailListByRoomNumberList(roomNumberList: string[], hotelId: number): Promise<BookingDetail[]> {
    return await Promise.all(roomNumberList.map(async roomNumber => await this.findBookingDetailByRoomNumber(roomNumber, hotelId)));
  }

  public async getUniqueGuestInfoList(hotelId: number): Promise<GuestInfo[]> {
    try {
      const bookingDetailList = await this.find({
        hotelId: hotelId
      })

      const guestInfoList = bookingDetailList.map(bookingDetail => {
        return bookingDetail.guestInfo;
      })

      return R.uniqBy(guestInfo => {
        return guestInfo.name;
      }, guestInfoList);
    } catch (error) {
      throw new DatabaseError("Error in function getUniqueGuestInfoList, error message: " + error.message);
    }
  }

  public async listGuestByAge(sign: Sign, guestAge: number, hotelId: number): Promise<GuestInfo[]> {
    const guestInfoList = await this.getUniqueGuestInfoList(hotelId);

    return guestInfoList.filter(guestInfo => {
      switch (sign) {
        case '<': return guestInfo.age < guestAge;
        case '>': return guestInfo.age > guestAge;
        case '=': return guestInfo.age = guestAge;
      }
    })
  }
}

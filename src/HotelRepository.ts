import { Hotel } from "./entity/Hotel";
import { EntityRepository, Repository } from "typeorm";
import { DatabaseError } from "./DatabaseError";

@EntityRepository(Hotel)
export class HotelRepository extends Repository<Hotel>{
  public async generateId(): Promise<number> {
    const hotels = await this.find();

    return hotels.length + 1;
  }

  public async findHotelById(hotelId: number): Promise<Hotel> {
    try {
      return await this.findOneOrFail({
        id: hotelId
      })
    } catch (error) {
      throw new DatabaseError("Error in function findHotelById, error message: " + error.message);
    }
  }

  public async createHotel(numberOfFloor: number, roomsPerFloor: number, hotelId: number): Promise<Hotel> {
    try {
      const hotel = await this.create({
        id: hotelId
      });

      return this.save(hotel);
    } catch (error) {
      throw new DatabaseError("Error in function createHotel, error message: " + error.message);
    }
  }
}
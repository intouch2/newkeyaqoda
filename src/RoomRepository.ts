import { Room } from "./entity/Room";
import { EntityRepository, Repository } from "typeorm";
import { SystemError } from "./SystemError";
import { DatabaseError } from "./DatabaseError";

@EntityRepository(Room)
export class RoomRepository extends Repository<Room> {
  public async createRooms(numberOfFloor: number, numberOfRoomsPerFloor: number, hotelId: number): Promise<Room[]> {
    const roomList = range(1, numberOfFloor)
      .flatMap(floorIndex => range(1, numberOfRoomsPerFloor)
        .map(roomsPerFloorIndex => {
          const roomNumber = (floorIndex).toString() + (roomsPerFloorIndex).toString().padStart(2, '0');

          return this.create({
            hotelId: hotelId,
            number: roomNumber,
            floor: floorIndex,
            isBooked: false
          })
        })
      );

    try {
      return await this.save(roomList);
    } catch (error) {
      throw new DatabaseError("Error in function createRooms, error message: " + error.message);
    }
  }

  public async getRoomListOnFloor(floorNumber: number, hotelId: number): Promise<Room[]> {
    try {
      return await this.find({
        hotelId: hotelId,
        floor: floorNumber
      });
    } catch (error) {
      throw new DatabaseError("Error in function getRoomListOnFloor, error message: " + error.message);
    }
  }

  public async findRoomByRoomNumber(roomNumber: string | undefined, hotelId: number): Promise<Room> {
    if (!roomNumber) {
      throw new SystemError("Trying to find a room by roomNumber that is null");
    }

    try {
      return await this.findOneOrFail({
        hotelId: hotelId,
        number: roomNumber
      });
    } catch (error) {
      throw new DatabaseError("Error in function findRoomByRoomNumber, error message: " + error.message);
    }
  }

  public async getAvailableRoomListOnFloor(floorNumber: number, hotelId: number): Promise<Room[]> {
    try {
      return await this.find({
        floor: floorNumber,
        hotelId: hotelId,
        isBooked: false
      });
    } catch (error) {
      throw new DatabaseError("Error in function getAvailableRoomListOnFloor, error message: " + error.message);
    }
  }

  public async getAvailableRoomNumberListOnFloor(floorNumber: number, hotelId: number): Promise<string[]> {
    const roomList = await this.getAvailableRoomListOnFloor(floorNumber, hotelId);

    return roomList.map(room => room.number);
  }

  public async getUnavailableRoomListOnFloor(floorNumber: number, hotelId: number): Promise<Room[]> {
    try {
      return await this.find({
        floor: floorNumber,
        hotelId: hotelId,
        isBooked: true
      });
    } catch (error) {
      throw new DatabaseError("Error in function getUnavailableRoomListOnFloor, error message: " + error.message);
    }
  }

  public async getUnavailableRoomNumberListOnFloor(floorNumber: number, hotelId: number): Promise<string[]> {
    const roomList = await this.getUnavailableRoomListOnFloor(floorNumber, hotelId);

    return roomList.map(room => room.number);
  }

  public async getAvailableRoomList(hotelId: number): Promise<Room[]> {
    try {
      return await this.find({
        hotelId: hotelId,
        isBooked: false
      });
    } catch (error) {
      throw new DatabaseError("Error in function getAvailableRoomList, error message: " + error.message);
    }
  }

  public async isFullyBooked(hotelId: number): Promise<boolean> {
    try {
      const rooms = await this.find({
        where: {
          hotelId: hotelId
        },
        select: ['isBooked']
      })

      return rooms.every(room => room.isBooked === true);
    } catch (error) {
      throw new DatabaseError("Error in function isFullyBooked, error message: " + error.message);
    }
  }

  public async updateOne(room: Room) {
    try {
      this.save(room);
    } catch (error) {
      throw new DatabaseError("Error in function updateOne, error message: " + error.message);
    }
  }

  public async updateMany(roomList: Room[]) {
    try {
      this.save(roomList);
    } catch (error) {
      throw new DatabaseError("Error in function updateMany, error message: " + error.message);
    }
  }
}

function range(start: number, end: number) {
  return [...Array(end - start + 1).keys()]
    .map(number => number + start);
}
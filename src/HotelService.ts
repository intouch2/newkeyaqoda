import { GuestInfo } from "./entity/GuestInfo";
import { BookingDetail } from "./entity/BookingDetail";
import { Room } from "./entity/Room";
import { BookDetail } from "./BookDetail"
import { Hotel } from "./entity/Hotel";
import { Sign } from "./Type";
import { SystemError } from "./SystemError";
import { Keycard } from "./entity/Keycard";
import { getCustomRepository } from "typeorm";
import { RoomRepository } from "./RoomRepository";
import { KeycardRepository } from "./KeycardRepository";
import { BookingDetailRepository } from "./BookingDetailRepository";
import { HotelRepository } from "./HotelRepository";
import * as R from "ramda";

export class HotelService {
  public async createHotel(numberOfFloor: number, numberOfRoomsPerFloor: number): Promise<Hotel> {
    const roomRepository = getCustomRepository(RoomRepository);
    const keycardRepository = getCustomRepository(KeycardRepository);
    const hotelRepository = getCustomRepository(HotelRepository);
    const totalNumberOfRooms = numberOfFloor * numberOfRoomsPerFloor;

    const hotelId = await hotelRepository.generateId();
    const hotel = await hotelRepository.createHotel(numberOfFloor, numberOfRoomsPerFloor, hotelId);

    await roomRepository.createRooms(numberOfFloor, numberOfRoomsPerFloor, hotelId);
    await keycardRepository.createKeycards(totalNumberOfRooms, hotelId)

    return hotel;
  }

  public async book(roomNumber: string, guestInfo: GuestInfo, hotelId: number): Promise<BookDetail> {
    const roomRepository = getCustomRepository(RoomRepository);
    const keycardRepository = getCustomRepository(KeycardRepository);
    const hotelRepository = getCustomRepository(HotelRepository);
    const bookingDetailRepository = getCustomRepository(BookingDetailRepository);
    const isFullyBooked = await roomRepository.isFullyBooked(hotelId);

    if (isFullyBooked) {
      throw new SystemError("Hotel is fully booked.");
    }

    const hotel = await hotelRepository.findHotelById(hotelId);
    const room = await roomRepository.findRoomByRoomNumber(roomNumber, hotelId);
    const keycard = await keycardRepository.availableKeycard(hotelId);

    try {
      const { checkedInRoom, bookingDetail, registeredKeycard } = hotel.book(room, guestInfo, keycard);

      await roomRepository.updateOne(checkedInRoom);
      await keycardRepository.saveOne(registeredKeycard);
      await bookingDetailRepository.addOne(bookingDetail);

      return { bookingDetail, registeredKeycard };
    }
    catch (error) {
      console.log(error);
      const bookingDetail = await bookingDetailRepository.findBookingDetailByRoomNumber(room.number, hotelId);

      throw new SystemError(`Cannot book room ${room.number} for ${guestInfo.name}, The room is currently booked by ${bookingDetail.guestInfo.name}`);
    }
  }

  public async bookRooms(roomList: Room[], guestInfo: GuestInfo, hotelId: number): Promise<BookDetail[]> {
    const roomRepository = getCustomRepository(RoomRepository);
    const keycardRepository = getCustomRepository(KeycardRepository);
    const hotelRepository = getCustomRepository(HotelRepository);
    const bookingDetailRepository = getCustomRepository(BookingDetailRepository);
    const hotel = await hotelRepository.findHotelById(hotelId);
    const numberOfKeycards = roomList.length;
    const keycardList = await keycardRepository.getAvailableKeycardList(hotelId, numberOfKeycards);

    const roomAndBookingDetailAndKeycardList = R.zip(roomList, keycardList).map(([room, keycard]) => {
      return hotel.book(room, guestInfo, keycard);
    });

    const { checkedInRoomList, registeredKeycardList, bookingDetailList } = roomAndBookingDetailAndKeycardList.reduce((previousValue, roomAndBookingDetailAndKeycard) => {
      const { checkedInRoom, registeredKeycard, bookingDetail } = roomAndBookingDetailAndKeycard
      const { checkedInRoomList, registeredKeycardList, bookingDetailList } = previousValue;

      return {
        checkedInRoomList: [...checkedInRoomList, checkedInRoom],
        registeredKeycardList: [...registeredKeycardList, registeredKeycard],
        bookingDetailList: [...bookingDetailList, bookingDetail]
      }
    }, { checkedInRoomList: [] as Room[], registeredKeycardList: [] as Keycard[], bookingDetailList: [] as BookingDetail[] })

    const bookDetail = roomAndBookingDetailAndKeycardList.map(roomAndBookingDetailAndKeycardList => {
      const { bookingDetail, registeredKeycard } = roomAndBookingDetailAndKeycardList

      return { bookingDetail, registeredKeycard };
    })

    await roomRepository.updateMany(checkedInRoomList);
    await keycardRepository.saveMany(registeredKeycardList);
    await bookingDetailRepository.addMany(bookingDetailList);

    return bookDetail;
  }

  public async bookOnFloor(floorNumber: number, guestInfo: GuestInfo, hotelId: number): Promise<BookDetail[]> {
    const roomRepository = getCustomRepository(RoomRepository);
    const roomList = await roomRepository.getRoomListOnFloor(floorNumber, hotelId);

    if (this.isFloorNotAvailable(roomList)) {
      throw new SystemError(`Cannot book floor ${floorNumber} for ${guestInfo.name}`);
    }

    const bookingDetailAndKeycardList = await this.bookRooms(roomList, guestInfo, hotelId);

    return bookingDetailAndKeycardList;
  }

  public async checkout(keycardNumber: string, guestName: string, hotelId: number): Promise<Room> {
    const roomRepository = getCustomRepository(RoomRepository);
    const keycardRepository = getCustomRepository(KeycardRepository);
    const hotelRepository = getCustomRepository(HotelRepository);
    const bookingDetailRepository = getCustomRepository(BookingDetailRepository);
    const hotel = await hotelRepository.findHotelById(hotelId);
    const keycard = await keycardRepository.findKeycardByCondition("keycardNumber", keycardNumber, hotelId);

    if (!keycard.roomNumber) {
      throw new SystemError("keycard is already available");
    }

    const room = await roomRepository.findRoomByRoomNumber(keycard.roomNumber, hotelId);
    const bookingDetail = await bookingDetailRepository.findBookingDetailByRoomNumber(room.number, hotelId);

    if (this.hasNoPermissionToCheckOut(bookingDetail, guestName)) {
      throw new SystemError(`Only ${bookingDetail.guestInfo.name} can checkout with keycard nummber ${keycard.number}.`);
    }

    const { checkedOutRoom, unregisteredKeycard } = hotel.checkout(room, keycard);

    await roomRepository.updateOne(checkedOutRoom);
    await keycardRepository.clearOne(unregisteredKeycard);
    await bookingDetailRepository.deleteOne(bookingDetail);

    return checkedOutRoom;
  }

  public async checkoutRooms(roomList: Room[], keycardList: Keycard[], bookingDetailList: BookingDetail[], hotelId: number): Promise<Room[]> {
    const roomRepository = getCustomRepository(RoomRepository);
    const keycardRepository = getCustomRepository(KeycardRepository);
    const hotelRepository = getCustomRepository(HotelRepository);
    const bookingDetailRepository = getCustomRepository(BookingDetailRepository);
    const hotel = await hotelRepository.findHotelById(hotelId);

    const checkedOutRoomAndUnregisteredKeycardList = R.zip(roomList, keycardList).map(([room, keycard]) => {
      return hotel.checkout(room, keycard);
    })

    const { checkedOutRoomList, unregisteredKeycardList } = checkedOutRoomAndUnregisteredKeycardList.reduce((previousValue, checkedOutRoomAndUnregisteredKeycard) => {
      const { checkedOutRoom, unregisteredKeycard } = checkedOutRoomAndUnregisteredKeycard
      const { checkedOutRoomList, unregisteredKeycardList } = previousValue;

      return {
        checkedOutRoomList: [...checkedOutRoomList, checkedOutRoom],
        unregisteredKeycardList: [...unregisteredKeycardList, unregisteredKeycard],
      }
    }, { checkedOutRoomList: [] as Room[], unregisteredKeycardList: [] as Keycard[] })

    await roomRepository.updateMany(checkedOutRoomList);
    await keycardRepository.clearMany(unregisteredKeycardList);
    await bookingDetailRepository.deleteMany(bookingDetailList);

    return checkedOutRoomList;
  }

  public async checkoutOnFloor(floorNumber: number, hotelId: number): Promise<Room[]> {
    const roomRepository = getCustomRepository(RoomRepository);
    const keycardRepository = getCustomRepository(KeycardRepository);
    const bookingDetailRepository = getCustomRepository(BookingDetailRepository);
    const roomList = await roomRepository.getUnavailableRoomListOnFloor(floorNumber, hotelId);
    const roomNumberList = roomList.map(room => room.number);
    const bookingDetailList = await bookingDetailRepository.getBookingDetailListByRoomNumberList(roomNumberList, hotelId);
    const keycardList = await keycardRepository.findKeycardListByRoomNumberList(roomNumberList, hotelId);

    const checkedOutRoomList = await this.checkoutRooms(roomList, keycardList, bookingDetailList, hotelId);

    return checkedOutRoomList;
  }

  public async listGuestByAge(sign: Sign, guestAge: number, hotelId: number): Promise<GuestInfo[]> {
    const bookingDetailRepository = getCustomRepository(BookingDetailRepository);
    const guestInfoList = await bookingDetailRepository.listGuestByAge(sign, guestAge, hotelId);

    return guestInfoList;
  }

  public async listGuestOnFloor(floorNumber: number, hotelId: number): Promise<GuestInfo[]> {
    const roomRepository = getCustomRepository(RoomRepository);
    const bookingDetailRepository = getCustomRepository(BookingDetailRepository);
    const roomNumberList = await roomRepository.getUnavailableRoomNumberListOnFloor(floorNumber, hotelId);
    const guestInfoList = await Promise.all(roomNumberList.map(async roomNumber => await bookingDetailRepository.getGuestInRoom(roomNumber, hotelId)));

    return guestInfoList;
  }

  public async listAvailableRoom(hotelId: number): Promise<Room[]> {
    const roomRepository = getCustomRepository(RoomRepository);
    const roomList = await roomRepository.getAvailableRoomList(hotelId);

    return roomList;
  }

  public async listGuest(hotelId: number): Promise<GuestInfo[]> {
    const bookingDetailRepository = getCustomRepository(BookingDetailRepository);
    const guestInfoList = await bookingDetailRepository.getUniqueGuestInfoList(hotelId);

    return guestInfoList;
  }

  public async getGuestInRoom(roomNumber: string, hotelId: number): Promise<GuestInfo> {
    const bookingDetailRepository = getCustomRepository(BookingDetailRepository);
    const guest = await bookingDetailRepository.getGuestInRoom(roomNumber, hotelId);

    return guest;
  }

  private hasNoPermissionToCheckOut(bookingDetail: BookingDetail, guestName: string): boolean {
    return bookingDetail.guestInfo.name !== guestName;
  }

  private isFloorNotAvailable(roomList: Room[]): boolean {
    return roomList.some(room => room.isBooked);
  }
}

export const hotelService = new HotelService();
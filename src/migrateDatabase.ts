import { readFileSync, readdirSync } from "fs";
import { getConnection } from "typeorm";

export async function migrateDatabase() {
    const sqlFiles =  readdirSync("migrations")

    const sqlCommands = sqlFiles.map(file => {
        const sqlCommand = readFileSync("migrations" + "/" + file, "utf-8");
        return sqlCommand
    });

    await sqlCommands.forEach(async content => {
        (await getConnection()).query(content);
    });
}
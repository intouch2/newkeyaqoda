import { Entity, PrimaryColumn, Column } from "typeorm";

@Entity('keycards')
export class Keycard {
    @PrimaryColumn({ name: 'hotel_id' })
    hotelId: number

    @PrimaryColumn()
    number: string

    @Column({ name: 'room_number', nullable: true })
    roomNumber?: string

    public get isAvailable(): boolean {
        return !this.roomNumber;
    }

    public register(roomNumber: string): Keycard {
        const keycard = new Keycard();

        keycard.hotelId = this.hotelId;
        keycard.number = this.number;
        keycard.roomNumber = roomNumber;

        return keycard;
    }

    public unregister(): Keycard {
        const keycard = new Keycard();

        keycard.hotelId = this.hotelId;
        keycard.number = this.number;
        keycard.roomNumber = 'undefined';

        return keycard;
    }
}

import { BookingDetail } from "./BookingDetail";
import { Room } from "./Room";
import { GuestInfo } from "./GuestInfo";
import { Keycard } from "./Keycard";
import { Entity, PrimaryColumn } from "typeorm";
import { SystemError } from "../SystemError";

@Entity('hotels')
export class Hotel {
  @PrimaryColumn()
  id: number

  public book(room: Room, guestInfo: GuestInfo, keycard: Keycard): { checkedInRoom: Room, bookingDetail: BookingDetail, registeredKeycard: Keycard } {
    if (room.isBooked) {
      throw new SystemError("Room is currently booked");
    }
    
    const bookingDetail = this.createBookingDetail(room.number, guestInfo);
    const registeredKeycard = keycard.register(room.number);
    const checkedInRoom = room.book();

    return { checkedInRoom, bookingDetail, registeredKeycard };
  }

  public checkout(room: Room, keycard: Keycard): { checkedOutRoom: Room, unregisteredKeycard: Keycard } {
    const checkedOutRoom = room.unbook();
    const unregisteredKeycard = keycard.unregister();

    return { checkedOutRoom, unregisteredKeycard };
  }

  private createBookingDetail(roomNumber: string, guestInfo: GuestInfo): BookingDetail {
    const bookingDetail = new BookingDetail();

    bookingDetail.roomNumber = roomNumber;
    bookingDetail.guestInfo = guestInfo;
    bookingDetail.hotelId = this.id;

    return bookingDetail;
  }
}
import { Entity, PrimaryColumn, Column } from 'typeorm'

@Entity('rooms')
export class Room {
  @PrimaryColumn({ name: 'hotel_id' })
  hotelId: number

  @PrimaryColumn()
  number: string

  @Column()
  floor: number

  @Column({ name: 'is_booked' })
  isBooked: boolean

  public book(): Room {
    const room = new Room();
    room.number = this.number;
    room.hotelId = this.hotelId;
    room.floor = this.floor;
    room.isBooked = true;

    return room;
  }

  public unbook(): Room {
    const room = new Room();
    room.number = this.number;
    room.hotelId = this.hotelId;
    room.floor = this.floor;
    room.isBooked = false;

    return room;
  }
}

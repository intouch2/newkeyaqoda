import { GuestInfo } from "./GuestInfo";
import { Entity, PrimaryColumn, Column } from "typeorm";

@Entity('booking_details')
export class BookingDetail {
  @PrimaryColumn({ name: 'hotel_id' })
  hotelId: number

  @PrimaryColumn({ name: 'guest_info', type: 'simple-json' })
  guestInfo: GuestInfo

  @Column({ name: 'room_number' })
  roomNumber: string
}

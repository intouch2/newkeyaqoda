import { Entity, PrimaryColumn } from "typeorm";

@Entity()
export class GuestInfo {
  @PrimaryColumn()
  name: string

  @PrimaryColumn()
  age: number
}

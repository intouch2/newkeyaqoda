export class SystemError extends Error {
    constructor(message: string) {
        super(`System Error: ${message}`)
    }
}
// import express, { Request, Response, response } from 'express'
// import bodyParser = require('body-parser');
// import { HotelService } from './HotelService';
// import { GuestInfo } from './GuestInfo';

// const app = express();
// const port = 3000;
// let hotelService: HotelService;

// app.use(bodyParser.json());

// app.post('/create_hotel', (request: Request, response: Response) => {
//   const { numberOfFloor, numberOfRoomsPerFloor } = request.body;
//   hotelService = new HotelService(parseInt(numberOfFloor), parseInt(numberOfRoomsPerFloor));

//   response.send(
//     `Hotel created with ${numberOfFloor} floor(s), ${numberOfRoomsPerFloor} room(s) per floor.`
//   );
// })

// app.post('/book', (request: Request, response: Response) => {
//   try {
//     const { roomNumber, guestName, guestAge } = request.body;
//     const guestInfo = new GuestInfo(guestName, parseInt(guestAge));
//     const { bookingDetail, registeredKeycard } = hotelService.book(roomNumber, guestInfo);
    
//     response.send(`Room ${roomNumber} is booked by ${bookingDetail.guestInfo.name} with keycard number ${registeredKeycard.number}.`);
//   }
//   catch (error) {
//     response.send(error.message);
//   }
// })

// app.post('/book_by_floor', (request: Request, response: Response) => {
//   try {
//     const { floorNumber, guestName, guestAge } = request.body;
//     const keycardAndRoomNumberList = hotelService.bookByFloor(floorNumber, guestName, parseInt(guestAge));
//     const roomNumberList = keycardAndRoomNumberList.map(data => data.roomNumber);
//     const keycardNumberList = keycardAndRoomNumberList.map(data => data.keycardNumber);

//     response.send(`Room ${roomNumberList.join(", ")} are booked with keycard number ${keycardNumberList.join(", ")}`);
//   }
//   catch(error) {
//     response.send(error.message);
//   }
// })

// app.post('/checkout', (request: Request, response: Response) => {
//   try {
//     const { keycardNumber, guestName } = request.body;
//     const checkedOutRoom = hotelService.checkout(keycardNumber, guestName);

//     response.send(`Room ${checkedOutRoom.number} is checkout.`);
//   }
//   catch (error) {
//     response.send(error);
//   }
// })

// app.post('/checkout_guest_by_floor', (request: Request, response: Response) => {
//   try {
//     const { floorNumber } = request.body;
//     const roomNumberList = hotelService.checkoutByFloor(floorNumber);

//     response.send(`Room ${roomNumberList.join(", ")} are checkout.`);
//   }
//   catch (error) {
//     response.send(error);
//   }
// })

// app.get('/list_available_rooms', (request: Request, response: Response) => {
//   const roomNumberList = hotelService.listAvailableRoomNumber;

//   response.send(`${roomNumberList.join(', ')}`);
// })

// app.get('/list_guest', (request: Request, response: Response) => {
//   const guestNameList = hotelService.listGuestName;

//   response.send(`${guestNameList.join(', ')}`);
// })

// app.get('/get_guest_in_room/:roomNumber', (request: Request, response: Response) => {
//   const { roomNumber } = request.params;
//   console.log(roomNumber);
//   const bookingDetail = hotelService.getGuestNameInTheRoom(roomNumber);

//   response.send(bookingDetail.guestInfo.name);
// })

// app.get('/list_guest_by_age/:sign/:guestAge', (request: Request, response: Response) => {
//   const { sign, guestAge } = request.params;
//   const guestNameList = hotelService.listGuestNameByAge(sign, parseInt(guestAge));

//   response.send(`${guestNameList.join(', ')}`);
// })

// app.get('/list_guest_by_floor/:floorNumber', (request: Request, response: Response) => {
//   const { floorNumber } = request.params;
//   const guestNameList = hotelService.listGuestNameOnFloor(floorNumber);

//   response.send(`${guestNameList.join(', ')}`);
// })

// app.listen(port, () => console.log(`App list on port ${port}`));
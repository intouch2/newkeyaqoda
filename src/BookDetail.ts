import { Keycard } from "./entity/Keycard";
import { BookingDetail } from "./entity/BookingDetail";

export interface BookDetail { 
    bookingDetail: BookingDetail;
    registeredKeycard: Keycard;
}
  
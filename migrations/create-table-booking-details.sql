CREATE TABLE IF NOT EXISTS booking_details (
    hotel_id integer NOT NULL,
    room_number text NOT NULL,
    guestInfo text NOT NULL,
    PRIMARY KEY (hotel_id, guestInfo)
)
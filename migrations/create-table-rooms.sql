CREATE TABLE IF NOT EXISTS rooms (
    hotel_id integer NOT NULL,
    number text NOT NULL,
    floor integer NOT NULL,
    is_booked boolean NOT NULL,
    PRIMARY KEY (hotel_id, number)
)
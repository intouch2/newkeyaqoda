CREATE TABLE IF NOT EXISTS hotels (
    id integer NOT NULL,
    number_of_floors integer NOT NULL,
    number_of_rooms_per_floor integer NOT NULL,
    PRIMARY KEY (id)
)

CREATE TABLE IF NOT EXISTS keycards (
    hotel_id integer NOT NULL,
    number text NOT NULL,
    room_number text NULL DEFAULT NULL,
    PRIMARY KEY (hotel_id, number)
);
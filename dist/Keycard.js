"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Keycard {
    constructor(number, hotelId, roomNumber = null) {
        this.number = number;
        this.hotelId = hotelId;
        this.roomNumber = roomNumber;
    }
    get isAvailable() {
        return this.roomNumber === null;
    }
    register(roomNumber) {
        return new Keycard(this.number, this.hotelId, roomNumber);
    }
    unregister() {
        return new Keycard(this.number, this.hotelId, null);
    }
}
exports.Keycard = Keycard;

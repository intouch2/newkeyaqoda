"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Room_1 = require("./entity/Room");
const typeorm_1 = require("typeorm");
const SystemError_1 = require("./SystemError");
const DatabaseError_1 = require("./DatabaseError");
let RoomRepository = class RoomRepository extends typeorm_1.Repository {
    async createRooms(numberOfFloor, numberOfRoomsPerFloor, hotelId) {
        const roomList = range(1, numberOfFloor)
            .flatMap(floorIndex => range(1, numberOfRoomsPerFloor)
            .map(roomsPerFloorIndex => {
            const roomNumber = (floorIndex).toString() + (roomsPerFloorIndex).toString().padStart(2, '0');
            return this.create({
                hotelId: hotelId,
                number: roomNumber,
                floor: floorIndex,
                isBooked: false
            });
        }));
        try {
            return await this.save(roomList);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function createRooms, error message: " + error.message);
        }
    }
    async getRoomListOnFloor(floorNumber, hotelId) {
        try {
            return await this.find({
                hotelId: hotelId,
                floor: floorNumber
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function getRoomListOnFloor, error message: " + error.message);
        }
    }
    async findRoomByRoomNumber(roomNumber, hotelId) {
        if (!roomNumber) {
            throw new SystemError_1.SystemError("Trying to find a room by roomNumber that is null");
        }
        try {
            return await this.findOneOrFail({
                hotelId: hotelId,
                number: roomNumber
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function findRoomByRoomNumber, error message: " + error.message);
        }
    }
    async getAvailableRoomListOnFloor(floorNumber, hotelId) {
        try {
            return await this.find({
                floor: floorNumber,
                hotelId: hotelId,
                isBooked: false
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function getAvailableRoomListOnFloor, error message: " + error.message);
        }
    }
    async getAvailableRoomNumberListOnFloor(floorNumber, hotelId) {
        const roomList = await this.getAvailableRoomListOnFloor(floorNumber, hotelId);
        return roomList.map(room => room.number);
    }
    async getUnavailableRoomListOnFloor(floorNumber, hotelId) {
        try {
            return await this.find({
                floor: floorNumber,
                hotelId: hotelId,
                isBooked: true
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function getUnavailableRoomListOnFloor, error message: " + error.message);
        }
    }
    async getUnavailableRoomNumberListOnFloor(floorNumber, hotelId) {
        const roomList = await this.getUnavailableRoomListOnFloor(floorNumber, hotelId);
        return roomList.map(room => room.number);
    }
    async getAvailableRoomList(hotelId) {
        try {
            return await this.find({
                hotelId: hotelId,
                isBooked: false
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function getAvailableRoomList, error message: " + error.message);
        }
    }
    async isFullyBooked(hotelId) {
        try {
            const rooms = await this.find({
                where: {
                    hotelId: hotelId
                },
                select: ['isBooked']
            });
            return rooms.every(room => room.isBooked === true);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function isFullyBooked, error message: " + error.message);
        }
    }
    async updateOne(room) {
        try {
            this.save(room);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function updateOne, error message: " + error.message);
        }
    }
    async updateMany(roomList) {
        try {
            this.save(roomList);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function updateMany, error message: " + error.message);
        }
    }
};
RoomRepository = __decorate([
    typeorm_1.EntityRepository(Room_1.Room)
], RoomRepository);
exports.RoomRepository = RoomRepository;
function range(start, end) {
    return [...Array(end - start + 1).keys()]
        .map(number => number + start);
}

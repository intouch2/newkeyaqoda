"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const SystemError_1 = require("./SystemError");
const typeorm_1 = require("typeorm");
const RoomManager_1 = require("./RoomManager");
const KeycardManager_1 = require("./KeycardManager");
const BookingDetailManager_1 = require("./BookingDetailManager");
const HotelManager_1 = require("./HotelManager");
const R = __importStar(require("ramda"));
class HotelService {
    async createHotel(numberOfFloor, numberOfRoomsPerFloor) {
        const roomRepository = typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
        const keycardRepository = typeorm_1.getCustomRepository(KeycardManager_1.KeycardRepository);
        const hotelRepository = typeorm_1.getCustomRepository(HotelManager_1.HotelRepository);
        const totalNumberOfRooms = numberOfFloor * numberOfRoomsPerFloor;
        const hotelId = await hotelRepository.generateId();
        const hotel = await hotelRepository.createHotel(numberOfFloor, numberOfRoomsPerFloor, hotelId);
        await roomRepository.createRooms(numberOfFloor, numberOfRoomsPerFloor, hotelId);
        await keycardRepository.createKeycards(totalNumberOfRooms, hotelId);
        return hotel;
    }
    async book(roomNumber, guestInfo, hotelId) {
        const roomRepository = typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
        const keycardRepository = typeorm_1.getCustomRepository(KeycardManager_1.KeycardRepository);
        const hotelRepository = typeorm_1.getCustomRepository(HotelManager_1.HotelRepository);
        const bookingDetailRepository = typeorm_1.getCustomRepository(BookingDetailManager_1.BookingDetailRepository);
        const isFullyBooked = await roomRepository.isFullyBooked(hotelId);
        if (isFullyBooked) {
            throw new Error("Hotel is fully booked.");
        }
        const hotel = await hotelRepository.findHotelById(hotelId);
        const room = await roomRepository.findRoomByRoomNumber(roomNumber, hotelId);
        const keycard = await keycardRepository.availableKeycard(hotelId);
        try {
            const { checkedInRoom, bookingDetail, registeredKeycard } = hotel.book(room, guestInfo, keycard);
            await roomRepository.updateOne(checkedInRoom);
            await keycardRepository.saveOne(registeredKeycard);
            await bookingDetailRepository.addOne(bookingDetail);
            return { bookingDetail, registeredKeycard };
        }
        catch (error) {
            console.log(error);
            const bookingDetail = await bookingDetailRepository.findBookingDetailByRoomNumber(room.number, hotelId);
            throw new SystemError_1.SystemError(`Cannot book room ${room.number} for ${guestInfo.name}, The room is currently booked by ${bookingDetail.guestInfo.name}`);
        }
    }
    async bookRooms(roomList, guestInfo, hotelId) {
        const roomRepository = typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
        const keycardRepository = typeorm_1.getCustomRepository(KeycardManager_1.KeycardRepository);
        const hotelRepository = typeorm_1.getCustomRepository(HotelManager_1.HotelRepository);
        const bookingDetailRepository = typeorm_1.getCustomRepository(BookingDetailManager_1.BookingDetailRepository);
        const hotel = await hotelRepository.findHotelById(hotelId);
        const numberOfKeycards = roomList.length;
        const keycardList = await keycardRepository.getAvailableKeycardList(hotelId, numberOfKeycards);
        const roomAndBookingDetailAndKeycardList = R.zip(roomList, keycardList).map(([room, keycard]) => {
            return hotel.book(room, guestInfo, keycard);
        });
        const { checkedInRoomList, registeredKeycardList, bookingDetailList } = roomAndBookingDetailAndKeycardList.reduce((previousValue, roomAndBookingDetailAndKeycard) => {
            const { checkedInRoom, registeredKeycard, bookingDetail } = roomAndBookingDetailAndKeycard;
            const { checkedInRoomList, registeredKeycardList, bookingDetailList } = previousValue;
            return {
                checkedInRoomList: [...checkedInRoomList, checkedInRoom],
                registeredKeycardList: [...registeredKeycardList, registeredKeycard],
                bookingDetailList: [...bookingDetailList, bookingDetail]
            };
        }, { checkedInRoomList: [], registeredKeycardList: [], bookingDetailList: [] });
        const bookDetail = roomAndBookingDetailAndKeycardList.map(roomAndBookingDetailAndKeycardList => {
            const { bookingDetail, registeredKeycard } = roomAndBookingDetailAndKeycardList;
            return { bookingDetail, registeredKeycard };
        });
        await roomRepository.updateMany(checkedInRoomList);
        await keycardRepository.saveMany(registeredKeycardList);
        await bookingDetailRepository.addMany(bookingDetailList);
        return bookDetail;
    }
    async bookOnFloor(floorNumber, guestInfo, hotelId) {
        const roomRepository = typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
        const roomList = await roomRepository.getRoomListOnFloor(floorNumber, hotelId);
        if (this.isFloorNotAvailable(roomList)) {
            throw new SystemError_1.SystemError(`Cannot book floor ${floorNumber} for ${guestInfo.name}`);
        }
        const bookingDetailAndKeycardList = await this.bookRooms(roomList, guestInfo, hotelId);
        return bookingDetailAndKeycardList;
    }
    async checkout(keycardNumber, guestName, hotelId) {
        const roomRepository = typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
        const keycardRepository = typeorm_1.getCustomRepository(KeycardManager_1.KeycardRepository);
        const hotelRepository = typeorm_1.getCustomRepository(HotelManager_1.HotelRepository);
        const bookingDetailRepository = typeorm_1.getCustomRepository(BookingDetailManager_1.BookingDetailRepository);
        const hotel = await hotelRepository.findHotelById(hotelId);
        const keycard = await keycardRepository.findKeycardByCondition("keycardNumber", keycardNumber, hotelId);
        if (!keycard.roomNumber) {
            throw new SystemError_1.SystemError("keycard is already available");
        }
        const room = await roomRepository.findRoomByRoomNumber(keycard.roomNumber, hotelId);
        const bookingDetail = await bookingDetailRepository.findBookingDetailByRoomNumber(room.number, hotelId);
        if (this.hasNoPermissionToCheckOut(bookingDetail, guestName)) {
            throw new SystemError_1.SystemError(`Only ${bookingDetail.guestInfo.name} can checkout with keycard nummber ${keycard.number}.`);
        }
        const { checkedOutRoom, unregisteredKeycard } = hotel.checkout(room, keycard);
        await roomRepository.updateOne(checkedOutRoom);
        await keycardRepository.clearOne(unregisteredKeycard);
        await bookingDetailRepository.deleteOne(bookingDetail);
        return checkedOutRoom;
    }
    async checkoutRooms(roomList, keycardList, bookingDetailList, hotelId) {
        const roomRepository = typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
        const keycardRepository = typeorm_1.getCustomRepository(KeycardManager_1.KeycardRepository);
        const hotelRepository = typeorm_1.getCustomRepository(HotelManager_1.HotelRepository);
        const bookingDetailRepository = typeorm_1.getCustomRepository(BookingDetailManager_1.BookingDetailRepository);
        const hotel = await hotelRepository.findHotelById(hotelId);
        const checkedOutRoomAndUnregisteredKeycardList = R.zip(roomList, keycardList).map(([room, keycard]) => {
            return hotel.checkout(room, keycard);
        });
        const { checkedOutRoomList, unregisteredKeycardList } = checkedOutRoomAndUnregisteredKeycardList.reduce((previousValue, checkedOutRoomAndUnregisteredKeycard) => {
            const { checkedOutRoom, unregisteredKeycard } = checkedOutRoomAndUnregisteredKeycard;
            const { checkedOutRoomList, unregisteredKeycardList } = previousValue;
            return {
                checkedOutRoomList: [...checkedOutRoomList, checkedOutRoom],
                unregisteredKeycardList: [...unregisteredKeycardList, unregisteredKeycard],
            };
        }, { checkedOutRoomList: [], unregisteredKeycardList: [] });
        await roomRepository.updateMany(checkedOutRoomList);
        await keycardRepository.clearMany(unregisteredKeycardList);
        await bookingDetailRepository.deleteMany(bookingDetailList);
        return checkedOutRoomList;
    }
    async checkoutOnFloor(floorNumber, hotelId) {
        const roomRepository = typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
        const keycardRepository = typeorm_1.getCustomRepository(KeycardManager_1.KeycardRepository);
        const bookingDetailRepository = typeorm_1.getCustomRepository(BookingDetailManager_1.BookingDetailRepository);
        const roomList = await roomRepository.getUnavailableRoomListOnFloor(floorNumber, hotelId);
        const roomNumberList = roomList.map(room => room.number);
        const bookingDetailList = await bookingDetailRepository.getBookingDetailListByRoomNumberList(roomNumberList, hotelId);
        const keycardList = await keycardRepository.findKeycardListByRoomNumberList(roomNumberList, hotelId);
        const checkedOutRoomList = await this.checkoutRooms(roomList, keycardList, bookingDetailList, hotelId);
        return checkedOutRoomList;
    }
    async listGuestByAge(sign, guestAge, hotelId) {
        const bookingDetailRepository = typeorm_1.getCustomRepository(BookingDetailManager_1.BookingDetailRepository);
        const guestInfoList = await bookingDetailRepository.listGuestByAge(sign, guestAge, hotelId);
        return guestInfoList;
    }
    async listGuestOnFloor(floorNumber, hotelId) {
        const roomRepository = typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
        const bookingDetailRepository = typeorm_1.getCustomRepository(BookingDetailManager_1.BookingDetailRepository);
        const roomNumberList = await roomRepository.getUnavailableRoomNumberListOnFloor(floorNumber, hotelId);
        const guestInfoList = await Promise.all(roomNumberList.map(async (roomNumber) => await bookingDetailRepository.getGuestInRoom(roomNumber, hotelId)));
        return guestInfoList;
    }
    async listAvailableRoom(hotelId) {
        const roomRepository = typeorm_1.getCustomRepository(RoomManager_1.RoomRepository);
        const roomList = await roomRepository.getAvailableRoomList(hotelId);
        return roomList;
    }
    async listGuest(hotelId) {
        const bookingDetailRepository = typeorm_1.getCustomRepository(BookingDetailManager_1.BookingDetailRepository);
        const guestInfoList = await bookingDetailRepository.getUniqueGuestInfoList(hotelId);
        return guestInfoList;
    }
    async getGuestInRoom(roomNumber, hotelId) {
        const bookingDetailRepository = typeorm_1.getCustomRepository(BookingDetailManager_1.BookingDetailRepository);
        const guest = await bookingDetailRepository.getGuestInRoom(roomNumber, hotelId);
        return guest;
    }
    hasNoPermissionToCheckOut(bookingDetail, guestName) {
        return bookingDetail.guestInfo.name !== guestName;
    }
    isFloorNotAvailable(roomList) {
        return roomList.some(room => room.isBooked);
    }
}
exports.HotelService = HotelService;
exports.hotelService = new HotelService();

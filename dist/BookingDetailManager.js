"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const BookingDetail_1 = require("./entity/BookingDetail");
const typeorm_1 = require("typeorm");
const R = __importStar(require("ramda"));
const DatabaseError_1 = require("./DatabaseError");
let BookingDetailRepository = class BookingDetailRepository extends typeorm_1.Repository {
    async findBookingDetailByRoomNumber(roomNumber, hotelId) {
        try {
            return await this.findOneOrFail({
                hotelId: hotelId,
                roomNumber: roomNumber
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function findBookingDetailByRoomNumber, error message: " + error.message);
        }
    }
    async addOne(bookingDetail) {
        try {
            await this.save(bookingDetail);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function addOne, error message: " + error.message);
        }
    }
    async addMany(bookingDetails) {
        try {
            await this.save(bookingDetails);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function addMany, error message: " + error.message);
        }
    }
    async deleteOne(bookingDetail) {
        try {
            await this.remove(bookingDetail);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function deleteOne, error message: " + error.message);
        }
    }
    async deleteMany(bookingDetails) {
        try {
            await this.remove(bookingDetails);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function deleteMany, error message: " + error.message);
        }
    }
    async getGuestInRoom(roomNumber, hotelId) {
        try {
            const bookingDetail = await this.findOneOrFail({
                roomNumber: roomNumber,
                hotelId: hotelId
            });
            return bookingDetail.guestInfo;
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function getGuestInRoom, error message: " + error.message);
        }
    }
    async getBookingDetailListByRoomNumberList(roomNumberList, hotelId) {
        return await Promise.all(roomNumberList.map(async (roomNumber) => await this.findBookingDetailByRoomNumber(roomNumber, hotelId)));
    }
    async getUniqueGuestInfoList(hotelId) {
        try {
            const bookingDetailList = await this.find({
                hotelId: hotelId
            });
            const guestInfoList = bookingDetailList.map(bookingDetail => {
                return bookingDetail.guestInfo;
            });
            return R.uniqBy(guestInfo => {
                return guestInfo.name;
            }, guestInfoList);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function getUniqueGuestInfoList, error message: " + error.message);
        }
    }
    async listGuestByAge(sign, guestAge, hotelId) {
        const guestInfoList = await this.getUniqueGuestInfoList(hotelId);
        return guestInfoList.filter(guestInfo => {
            switch (sign) {
                case '<': return guestInfo.age < guestAge;
                case '>': return guestInfo.age > guestAge;
                case '=': return guestInfo.age = guestAge;
            }
        });
    }
};
BookingDetailRepository = __decorate([
    typeorm_1.EntityRepository(BookingDetail_1.BookingDetail)
], BookingDetailRepository);
exports.BookingDetailRepository = BookingDetailRepository;

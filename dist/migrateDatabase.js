"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const typeorm_1 = require("typeorm");
async function migrateDatabase() {
    const sqlFiles = fs_1.readdirSync("migrations");
    const sqlCommands = sqlFiles.map(file => {
        const sqlCommand = fs_1.readFileSync("migrations" + "/" + file, "utf-8");
        return sqlCommand;
    });
    await sqlCommands.forEach(async (content) => {
        (await typeorm_1.getConnection()).query(content);
    });
}
exports.migrateDatabase = migrateDatabase;

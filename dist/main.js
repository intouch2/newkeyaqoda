"use strict";
// import { readFileSync } from "fs";
// import { Command } from "./Command"
// import { HotelService } from "./HotelService";
// import { GuestInfo } from "./GuestInfo";
// function main() {
//   const fileName = "input.txt";
//   const commands = getCommandsFromFileName(fileName);
//   let hotelService: HotelService;
//   commands.forEach(command => {
//     switch (command.name) {
//       case "create_hotel": {
//         const [numberOfFloor, numberOfRoomsPerFloor] = command.params;
//         hotelService = new HotelService(parseInt(numberOfFloor), parseInt(numberOfRoomsPerFloor));
//         console.log(
//           `Hotel created with ${numberOfFloor} floor(s), ${numberOfRoomsPerFloor} room(s) per floor.`
//         );
//         break;
//       }
//       case "book": {
//         try {
//           const [roomNumber, guestName, guestAge] = command.params;
//           const guestInfo = new GuestInfo(guestName, parseInt(guestAge));
//           const { bookingDetail, registeredKeycard } = hotelService.book(roomNumber, guestInfo);
//           console.log(`Room ${roomNumber} is booked by ${bookingDetail.guestInfo.name} with keycard number ${registeredKeycard.number}.`);
//         }
//         catch (error) {
//           console.log(error.message);
//         }
//         break;
//       }
//       case "checkout": {
//         try {
//           const [keycardNumber, guestName] = command.params;
//           const checkedOutRoom = hotelService.checkout(keycardNumber, guestName);
//           console.log(`Room ${checkedOutRoom.number} is checkout.`);
//         }
//         catch (error) {
//           console.log(error);
//         }
//         break;
//       }
//       case "list_available_rooms": {
//         const roomNumberList = hotelService.listAvailableRoomNumber;
//         console.log(`${roomNumberList.join(', ')}`);
//         break;
//       }
//       case "list_guest": {
//         const guestNameList = hotelService.listGuestName;
//         console.log(`${guestNameList.join(', ')}`);
//         break;
//       }
//       case "get_guest_in_room": {
//         const [roomNumber] = command.params;
//         const bookingDetail = hotelService.getGuestNameInTheRoom(roomNumber);
//         console.log(bookingDetail.guestInfo.name);
//         break;
//       }
//       case "list_guest_by_age": {
//         const [sign, guestAge] = command.params;
//         const guestNameList = hotelService.listGuestNameByAge(sign, parseInt(guestAge));
//         console.log(`${guestNameList.join(', ')}`);
//         break;
//       }
//       case "list_guest_by_floor": {
//         const [floorNumber] = command.params;
//         const guestNameList = hotelService.listGuestNameOnFloor(floorNumber);
//         console.log(`${guestNameList.join(', ')}`);
//         break;
//       }
//       case "checkout_guest_by_floor": {
//         try {
//           const [floorNumber] = command.params;
//           const roomList = hotelService.checkoutByFloor(floorNumber);
//           const roomNumberList = roomList.map(room => room.number);
//           console.log(`Room ${roomNumberList.join(", ")} are checkout.`);
//         }
//         catch (error) {
//           console.log(error);
//         }
//         break;
//       }
//       case "book_by_floor": {
//         try {
//           const [floorNumber, guestName, guestAge] = command.params;
//           const bookingDetailAndKeycardList = hotelService.bookByFloor(floorNumber, guestName, parseInt(guestAge));
//           const roomNumberList = bookingDetailAndKeycardList.map(data => data.bookingDetail.roomNumber);
//           const keycardNumberList = bookingDetailAndKeycardList.map(data => data.registeredKeycard.number);
//           console.log(`Room ${roomNumberList.join(", ")} are booked with keycard number ${keycardNumberList.join(", ")}`);
//         }
//         catch(error) {
//           console.log(error.message);
//         }
//         break;
//       }
//       default: {
//         break;
//       }
//     }
//   });
// }
// function getCommandsFromFileName(fileName: string) {
//   const file = readFileSync(fileName, "utf-8");
//   return file
//     .split("\n")
//     .map((line: string) => line.split(" "))
//     .map(
//       ([commandName, ...params]) =>
//         new Command(
//           commandName,
//           params.map((param) => {
//             return param;
//           })
//         )
//     );
// }
// main();

"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importStar(require("express"));
const bodyParser = require("body-parser");
const HotelService_1 = require("./HotelService");
const GuestInfo_1 = require("./entity/GuestInfo");
const SystemError_1 = require("./SystemError");
const migrateDatabase_1 = require("./migrateDatabase");
const typeorm_1 = require("typeorm");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const envResult = require('dotenv').config();
if (envResult.error) {
    throw new Error(envResult.error);
}
const port = process.env.PORT || 5000;
const secretKey = process.env.APP_KEY;
function needAuthorization(request, response, next) {
    try {
        const [_, token] = request.headers.authorization.split(' ');
        if (!token) {
            throw new Error("No authorization");
        }
        const payload = jsonwebtoken_1.default.verify(token, secretKey);
        ;
        request.$payload = payload;
    }
    catch (error) {
        response.status(400).json({ "Authorization error": error.message });
    }
    next();
}
function withHotelId(request, response, next) {
    try {
        const [_, hotelId] = request.headers.authorization.split(' ');
        ;
        request.$hotelId = (request.$payload).hotelId;
        next();
    }
    catch (error) {
        response.status(400).json("Probably problem with token, error: " + error.message);
    }
}
const hotelRouter = express_1.Router();
hotelRouter.use(bodyParser.json());
hotelRouter.use(needAuthorization, withHotelId);
typeorm_1.createConnection().then(async () => {
    console.log("DATABASE: CONNECTED SUCCESSFULLY");
    try {
        migrateDatabase_1.migrateDatabase();
    }
    catch (error) {
        express_1.response.json(400).json({ "migrate Database Error": error.message });
    }
    const app = express_1.default();
    app.use(bodyParser.json());
    app.post("/hotels", async (request, response) => {
        const { numberOfFloor, roomsPerFloor } = request.body;
        try {
            const hotel = await HotelService_1.hotelService.createHotel(numberOfFloor, roomsPerFloor);
            const token = jsonwebtoken_1.default.sign({
                hotelId: hotel.id.toString()
            }, secretKey, {
                algorithm: 'HS256', expiresIn: '1h', issuer: 'Hotel'
            });
            return response.json({
                "information": `Hotel created with ${numberOfFloor} floor(s), ${roomsPerFloor} room(s) per floor.`,
                "result": token,
            });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    hotelRouter.post("/rooms/book", async (request, response) => {
        const { roomNumber, guestName, guestAge } = request.body;
        const hotelRequest = request;
        const hotelId = hotelRequest.$hotelId;
        try {
            const guestInfo = new GuestInfo_1.GuestInfo();
            guestInfo.name = guestName;
            guestInfo.age = guestAge;
            const { bookingDetail, registeredKeycard } = await HotelService_1.hotelService.book(roomNumber, guestInfo, parseInt(hotelId));
            return response.json({ "result": { bookingDetail, registeredKeycard } });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    hotelRouter.post("/rooms/checkout", async (request, response) => {
        const { keycardNumber, guestName } = request.body;
        const hotelRequest = request;
        const hotelId = hotelRequest.$hotelId;
        try {
            const checkedOutRoom = await HotelService_1.hotelService.checkout(keycardNumber, guestName, parseInt(hotelId));
            return response.json({ "result": checkedOutRoom });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    hotelRouter.post("/rooms/checkout_guest_by_floor", async (request, response) => {
        const { floorNumber } = request.body;
        const hotelRequest = request;
        const hotelId = hotelRequest.$hotelId;
        try {
            const roomList = await HotelService_1.hotelService.checkoutOnFloor(floorNumber, parseInt(hotelId));
            return response.json({ "result": roomList });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    hotelRouter.post("/rooms/book_by_floor", async (request, response) => {
        const { floorNumber, guestName, guestAge } = request.body;
        const hotelRequest = request;
        const hotelId = hotelRequest.$hotelId;
        try {
            const guestInfo = new GuestInfo_1.GuestInfo();
            guestInfo.name = guestName;
            guestInfo.age = guestAge;
            const bookingDetailAndKeycardList = await HotelService_1.hotelService.bookOnFloor(floorNumber, guestInfo, parseInt(hotelId));
            return response.json({ "result": bookingDetailAndKeycardList });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    hotelRouter.get("/rooms/list_available", async (request, response) => {
        const hotelRequest = request;
        const hotelId = hotelRequest.$hotelId;
        try {
            const roomList = await HotelService_1.hotelService.listAvailableRoom(parseInt(hotelId));
            response.json({ "result": roomList });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    hotelRouter.get("/guests/list", async (request, response) => {
        const hotelRequest = request;
        const hotelId = hotelRequest.$hotelId;
        try {
            const guestList = await HotelService_1.hotelService.listGuest(parseInt(hotelId));
            response.json({ "result": guestList });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    hotelRouter.get("/rooms/get_guest_in_room/:roomNumber", async (request, response) => {
        const hotelRequest = request;
        const hotelId = hotelRequest.$hotelId;
        const { roomNumber } = request.params;
        try {
            const guest = await HotelService_1.hotelService.getGuestInRoom(roomNumber, parseInt(hotelId));
            response.json({ "result": guest });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    hotelRouter.get("/guests/list_guest_by_age/:condition/:targetAge", async (request, response) => {
        const hotelRequest = request;
        const hotelId = hotelRequest.$hotelId;
        const { condition, targetAge } = request.params;
        try {
            const guestList = await HotelService_1.hotelService.listGuestByAge(condition, targetAge, parseInt(hotelId));
            response.json({ "result": guestList });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    hotelRouter.get("/guests/list_guest_by_floor/:floorNumber", async (request, response) => {
        const hotelRequest = request;
        const hotelId = hotelRequest.$hotelId;
        const { floorNumber } = request.params;
        try {
            const guestList = await HotelService_1.hotelService.listGuestOnFloor(floorNumber, parseInt(hotelId));
            response.json({ "result": guestList });
        }
        catch (error) {
            if (error instanceof SystemError_1.SystemError)
                response.status(400).json({ message: error.message });
            else
                response.status(500).json({ message: error.message });
        }
    });
    app.use(hotelRouter);
    app.listen(port, () => console.log(`App listen on port ${port}.`));
});

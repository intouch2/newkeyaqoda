"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = require("pg");
exports.client = new pg_1.Client({
    host: '127.0.0.1',
    port: 5432,
    user: 'aqoda',
    database: 'aqoda'
});

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class GuestInfo {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}
exports.GuestInfo = GuestInfo;

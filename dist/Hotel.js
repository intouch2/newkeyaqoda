"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BookingDetail_1 = require("./BookingDetail");
class Hotel {
    constructor(id) {
        this.id = id;
    }
    book(room, guestInfo, keycard) {
        if (room.isBooked) {
            throw new Error("Room is currently booked");
        }
        const bookingDetail = this.createBookingDetail(room.number, guestInfo);
        const registeredKeycard = keycard.register(room.number);
        const checkedInRoom = room.book();
        return { checkedInRoom, bookingDetail, registeredKeycard };
    }
    checkout(room, keycard) {
        const checkedOutRoom = room.unbook();
        const unregisteredKeycard = keycard.unregister();
        return { checkedOutRoom, unregisteredKeycard };
    }
    createBookingDetail(roomNumber, guestInfo) {
        return new BookingDetail_1.BookingDetail(roomNumber, guestInfo, this.id);
    }
}
exports.Hotel = Hotel;

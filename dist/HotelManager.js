"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Hotel_1 = require("./entity/Hotel");
const typeorm_1 = require("typeorm");
const DatabaseError_1 = require("./DatabaseError");
let HotelRepository = class HotelRepository extends typeorm_1.Repository {
    async generateId() {
        const hotels = await this.find();
        return hotels.length + 1;
    }
    async findHotelById(hotelId) {
        try {
            return await this.findOneOrFail({
                id: hotelId
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function findHotelById, error message: " + error.message);
        }
    }
    async createHotel(numberOfFloor, roomsPerFloor, hotelId) {
        try {
            const hotel = await this.create({
                id: hotelId
            });
            return this.save(hotel);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function createHotel, error message: " + error.message);
        }
    }
};
HotelRepository = __decorate([
    typeorm_1.EntityRepository(Hotel_1.Hotel)
], HotelRepository);
exports.HotelRepository = HotelRepository;

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Keycard_1 = require("./entity/Keycard");
const typeorm_1 = require("typeorm");
const DatabaseError_1 = require("./DatabaseError");
let KeycardRepository = class KeycardRepository extends typeorm_1.Repository {
    async createKeycards(totalNumberOfRooms, hotelId) {
        const keycardList = range(1, totalNumberOfRooms)
            .map((keycardNumberIndex) => {
            const keycardNumber = keycardNumberIndex;
            return this.create({
                hotelId: hotelId,
                number: keycardNumber.toString(),
                roomNumber: undefined
            });
        });
        try {
            return await this.save(keycardList);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function createKeycards, error message: " + error.message);
        }
    }
    async availableKeycard(hotelId) {
        try {
            return await this.findOneOrFail({
                where: {
                    roomNumber: null,
                    hotelId: hotelId
                },
                order: {
                    number: 'ASC'
                }
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function availableKeycard, error message: " + error.message);
        }
    }
    async getAvailableKeycardList(hotelId, numberOfKeycards) {
        try {
            return await this.find({
                where: {
                    roomNumber: null,
                    hotelId: hotelId
                },
                order: {
                    number: 'ASC'
                },
                take: numberOfKeycards
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function getAvailableKeycardList, error message: " + error.message);
        }
    }
    async findKeycardByCondition(condition, number, hotelId) {
        if (condition === "keycardNumber") {
            return await this.findOneOrFail({
                number: number,
                hotelId: hotelId
            });
        }
        else if (condition === "roomNumber") {
            return await this.findOneOrFail({
                roomNumber: number,
                hotelId: hotelId
            });
        }
        else {
            throw new Error(`Couldn't find keycard ${number} by ${condition}`);
        }
    }
    async findKeycardListByRoomNumberList(roomNumberList, hotelId) {
        try {
            return await this.find({
                hotelId: hotelId,
                roomNumber: typeorm_1.In(roomNumberList)
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function findKeycardListByRoomNumberList, error message: " + error.message);
        }
    }
    async saveOne(keycard) {
        try {
            this.save(keycard);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function saveOne, error message: " + error.message);
        }
    }
    async saveMany(keycardList) {
        try {
            this.save(keycardList);
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function saveMany, error message: " + error.message);
        }
    }
    async clearOne(keycard) {
        try {
            await this.update({
                hotelId: keycard.hotelId,
                number: keycard.number
            }, {
                roomNumber: undefined
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function clearOne, error message: " + error.message);
        }
    }
    async clearMany(keycardList) {
        try {
            const hotelId = keycardList[0].hotelId;
            const keycardNumberList = keycardList.map(keycard => keycard.number);
            await this.update({
                hotelId: hotelId,
                number: typeorm_1.In(keycardNumberList)
            }, {
                roomNumber: undefined
            });
        }
        catch (error) {
            throw new DatabaseError_1.DatabaseError("Error in function clearMany, error message: " + error.message);
        }
    }
};
KeycardRepository = __decorate([
    typeorm_1.EntityRepository(Keycard_1.Keycard)
], KeycardRepository);
exports.KeycardRepository = KeycardRepository;
function range(start, end) {
    return [...Array(end - start + 1).keys()]
        .map(number => number + start);
}

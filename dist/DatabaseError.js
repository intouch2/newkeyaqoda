"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class DatabaseError extends Error {
    constructor(message) {
        super(`Database Error: ${message}`);
    }
}
exports.DatabaseError = DatabaseError;

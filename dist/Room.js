"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Room {
    constructor(number, hotelId, floor, isBooked = false) {
        this.number = number;
        this.hotelId = hotelId;
        this.floor = floor;
        this.isBooked = isBooked;
    }
    book() {
        return new Room(this.number, this.hotelId, this.floor, true);
    }
    unbook() {
        return new Room(this.number, this.hotelId, this.floor, false);
    }
}
exports.Room = Room;

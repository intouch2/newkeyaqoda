"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BookingDetail {
    constructor(roomNumber, guestInfo, hotelId) {
        this.roomNumber = roomNumber;
        this.guestInfo = guestInfo;
        this.hotelId = hotelId;
    }
}
exports.BookingDetail = BookingDetail;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SystemError extends Error {
    constructor(message) {
        super(`System Error: ${message}`);
    }
}
exports.SystemError = SystemError;

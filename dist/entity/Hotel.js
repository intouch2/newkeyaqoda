"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const BookingDetail_1 = require("./BookingDetail");
const typeorm_1 = require("typeorm");
let Hotel = class Hotel {
    book(room, guestInfo, keycard) {
        if (room.isBooked) {
            throw new Error("Room is currently booked");
        }
        const bookingDetail = this.createBookingDetail(room.number, guestInfo);
        const registeredKeycard = keycard.register(room.number);
        const checkedInRoom = room.book();
        return { checkedInRoom, bookingDetail, registeredKeycard };
    }
    checkout(room, keycard) {
        const checkedOutRoom = room.unbook();
        const unregisteredKeycard = keycard.unregister();
        return { checkedOutRoom, unregisteredKeycard };
    }
    createBookingDetail(roomNumber, guestInfo) {
        const bookingDetail = new BookingDetail_1.BookingDetail();
        bookingDetail.roomNumber = roomNumber;
        bookingDetail.guestInfo = guestInfo;
        bookingDetail.hotelId = this.id;
        return bookingDetail;
    }
};
__decorate([
    typeorm_1.PrimaryColumn(),
    __metadata("design:type", Number)
], Hotel.prototype, "id", void 0);
Hotel = __decorate([
    typeorm_1.Entity('hotels')
], Hotel);
exports.Hotel = Hotel;

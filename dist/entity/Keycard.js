"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Keycard_1;
Object.defineProperty(exports, "__esModule", { value: true });
"use strict";
const typeorm_1 = require("typeorm");
let Keycard = Keycard_1 = class Keycard {
    get isAvailable() {
        return !this.roomNumber;
    }
    register(roomNumber) {
        const keycard = new Keycard_1();
        keycard.hotelId = this.hotelId;
        keycard.number = this.number;
        keycard.roomNumber = roomNumber;
        return keycard;
    }
    unregister() {
        const keycard = new Keycard_1();
        keycard.hotelId = this.hotelId;
        keycard.number = this.number;
        keycard.roomNumber = 'undefined';
        return keycard;
    }
};
__decorate([
    typeorm_1.PrimaryColumn({ name: 'hotel_id' }),
    __metadata("design:type", Number)
], Keycard.prototype, "hotelId", void 0);
__decorate([
    typeorm_1.PrimaryColumn(),
    __metadata("design:type", String)
], Keycard.prototype, "number", void 0);
__decorate([
    typeorm_1.Column({ name: 'room_number', nullable: true }),
    __metadata("design:type", String)
], Keycard.prototype, "roomNumber", void 0);
Keycard = Keycard_1 = __decorate([
    typeorm_1.Entity('keycards')
], Keycard);
exports.Keycard = Keycard;

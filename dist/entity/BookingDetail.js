"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const GuestInfo_1 = require("./GuestInfo");
const typeorm_1 = require("typeorm");
let BookingDetail = class BookingDetail {
};
__decorate([
    typeorm_1.PrimaryColumn({ name: 'hotel_id' }),
    __metadata("design:type", Number)
], BookingDetail.prototype, "hotelId", void 0);
__decorate([
    typeorm_1.PrimaryColumn({ name: 'guest_info', type: 'simple-json' }),
    __metadata("design:type", GuestInfo_1.GuestInfo)
], BookingDetail.prototype, "guestInfo", void 0);
__decorate([
    typeorm_1.Column({ name: 'room_number' }),
    __metadata("design:type", String)
], BookingDetail.prototype, "roomNumber", void 0);
BookingDetail = __decorate([
    typeorm_1.Entity('booking_details')
], BookingDetail);
exports.BookingDetail = BookingDetail;

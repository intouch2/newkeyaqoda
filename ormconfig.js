module.exports = {
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'aqoda',
    password: '',
    database: 'aqoda',
    synchronize: false,
    entities: ['dist/entity/*.js']
  }
  